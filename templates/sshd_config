#       $OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/bin:/bin:/usr/sbin:/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

Include {{ sshd_include_files }}

Port {{ sshd_port }}
#AddressFamily {{ sshd_address_family }}
ListenAddress {{ sshd_listen_address }}
#ListenAddress {{ sshd_listen_address_ipv6 }}

#HostKey {{ sshd_hostkey_type_rsa }}
#HostKey {{ sshd_hostkey_type_ecdsa }}
#HostKey {{ sshd_hostkey_type_es25519 }}

# Ciphers and keying
#RekeyLimit {{ sshd_rekey_limit }}

# Logging
#SyslogFacility {{ sshd_syslog_facility }}
LogLevel {{ sshd_log_level }}

# Authentication:

#LoginGraceTime {{ sshd_login_grace_time }}
PermitRootLogin {{ sshd_permit_root_login }}
#StrictModes {{ sshd_strict_modes }}
MaxAuthTries {{ sshd_max_auth_tries }}
#MaxSessions {{ sshd_max_sessions }}

PubkeyAuthentication {{ sshd_pubkey_authentication }}

# Expect .ssh/authorized_keys2 to be disregarded by default in future.
#AuthorizedKeysFile    {{ sshd_authorized_keys_file }}

#AuthorizedPrincipalsFile {{ sshd_authorized_principals_file }}

#AuthorizedKeysCommand {{ sshd_authorized_keys_command }}
#AuthorizedKeysCommandUser {{ sshd_authorized_keys_command_user }}

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication {{ sshd_host_based_authentication }}
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts {{ sshd_ignore_user_known_hosts }}
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts {{ sshd_ignore_rhosts }}

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication {{ sshd_password_authentication }}
#PermitEmptyPasswords {{ sshd_permit_empty_passwords }}

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication {{ sshd_challenge_response_authentication }}

# Kerberos options
#KerberosAuthentication {{ sshd_kerberos_authentication }}
#KerberosOrLocalPasswd {{ sshd_kerberos_or_local_passwd }}
#KerberosTicketCleanup {{ sshd_kerberos_ticket_cleanup }}
#KerberosGetAFSToken {{ sshd_kerberos_get_afs_token }}

# GSSAPI options
#GSSAPIAuthentication {{ sshd_gssapi_authentication }}
#GSSAPICleanupCredentials {{ sshd_gssapi_cleanup_credentials }}
#GSSAPIStrictAcceptorCheck {{ sshd_gssapi_strict_acceptor_check }}
#GSSAPIKeyExchange {{ sshd_gssapi_key_exchange }}

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM {{ sshd_use_pam }}

#AllowAgentForwarding {{ sshd_allow_agent_forwarding }}
#AllowTcpForwarding {{ sshd_allow_tcp_forwarding }}
#GatewayPorts {{ sshd_gateway_ports }}
X11Forwarding {{ sshd_x11_forwarding }}
#X11DisplayOffset {{ sshd_x11_display_offset }}
#X11UseLocalhost {{ sshd_x11_use_local_host }}
#PermitTTY {{ sshd_permit_tty }}
PrintMotd {{ sshd_print_motd }}
#PrintLastLog {{ sshd_print_last_log }}
#TCPKeepAlive {{ sshd_tcp_keep_alive }}
#PermitUserEnvironment {{ sshd_permit_user_environment }}
#Compression delayed
ClientAliveInterval {{ sshd_client_alive_interval }}
ClientAliveCountMax {{ sshd_client_alive_countmax }}
#UseDNS {{ sshd_use_dns }}
#PidFile {{ sshd_pid_file }}
#MaxStartups {{ sshd_max_startups }}
#PermitTunnel {{ sshd_permit_tunnel }}
#ChrootDirectory {{ sshd_chroot_directory }}
#VersionAddendum {{ sshd_version_addendum }}

# no default banner path
#Banner none

# Allow client to pass locale environment variables
AcceptEnv {{ sshd_accept_env }}

# override default of no subsystems
Subsystem       {{ sshd_subsystem }}

# Example of overriding settings on a per-user basis
#Match User anoncvs
#       X11Forwarding no
#       AllowTcpForwarding no
#       PermitTTY no
#       ForceCommand cvs server