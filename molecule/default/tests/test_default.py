import os
import testinfra.utils.ansible_runner
import pwd

pkg_name='openssh-server' and 'openssh-client'
svc_name='ssh'
ssh_folder='/etc/ssh'

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")

def test_ssh_directory_exists(host):
    ssh_dir_exists = host.file(ssh_folder).exists
    assert ssh_dir_exists

def ssh_is_installed(host):
    ssh = host.package(pkg_name)
    assert ssh.is_installed

def ssh_running_and_enabled(host):
    ssh = host.service(svc_name)
    assert ssh.is_running
    assert ssh.is_enabled
